Feature: Enrollment (inscrição)

Um estudante que quer participar de um curso pode reservar uma vaga.
A vaga fica reservada ate a confirmação do pagamento.

Rule: Estudantes podem se inscrever quando tiver vagas

Scenario: Estudante reserva uma vaga em um curso com vagas em aberto
	Given um estudante que quer participar de um curso
	And o curso tem ainda 5 vagas em aberto
	When o estudante reserva sua vaga
	Then a vaga deveria estar marcada para esperando pagamento
	And o curso deveria ter somente 4 vagas em aberto