using TechTalk.SpecFlow;
using FluentAssertions;
using System;
using TechTalk.SpecFlow.Assist;
using System.Collections.Generic;
using System.Linq;

namespace CourseApp
{
    [Binding]
    public sealed class CourseEnrollmentStepDefinitions
    {
        string nomeEstudanteQuerComprarCurso;
        CourseManager courseManager;
        string reservationId;


        [Given("um estudante que quer participar de um curso")]
        public void GivenUmEstudanteQuerParticiparDeUmCurso()
        {
            nomeEstudanteQuerComprarCurso = "João";
        }

        [Given("o curso tem ainda (.*) vagas em aberto")]
        public void WhenCursoTemAindaVagasEmAberto(int vagasEmAberto)
        {
            courseManager = new CourseManager();
            courseManager.createCourse("A-CSD-001", "A-CSD", "28-9-2021", vagasEmAberto);
        }

        [When("o estudante reserva sua vaga")]
        public void WhenEstudanteReservaVaga()
        {
            reservationId = courseManager.createReservation("A-CSD-001", nomeEstudanteQuerComprarCurso, "Joao123@gmail.com");
        }

        [Then("a vaga deveria estar marcada para esperando pagamento")]
        public void ThenVagaDeveriaEstarMarcadaParaEsperandoPagamento()
        {
            Reservation vagaReservada = courseManager.getReservationByCourseAndReservationId("A-CSD-001", reservationId);
            vagaReservada.Should().NotBeNull();
        }

        [Then("o curso deveria ter somente (.*) vagas em aberto")]
        public void ThenCursoDeveriaTerSomenteVagasEmAberto(int numberOfSeats)
        {
            courseManager.getNumberOfSeats("A-CSD-001").Should().Be(numberOfSeats);
        }

    }
}