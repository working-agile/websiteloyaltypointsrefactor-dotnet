using System;
using System.Collections.Generic;

namespace CourseApp
{
    public class CourseManager
    {
        private Dictionary<String, Course> dictionaryCourses { set; get; }

        public CourseManager()
        {
            dictionaryCourses = new Dictionary<String, Course>();
        }

        public void createCourse(String courseId, String nomeCurso, String dataInicio, int numeroDeVagas)
        {
            Course novoCurso = new Course(courseId, nomeCurso, dataInicio, numeroDeVagas);
            dictionaryCourses.Add(courseId, novoCurso);
        }

        public Course getCourse(String codigoCurso)
        {
            return dictionaryCourses[codigoCurso];
        }

        public String createReservation(String courseId, String nomeEstudante, String emailEstudante)
        {
            Course course = dictionaryCourses[courseId];
            return course.createReservation(nomeEstudante, emailEstudante);
        }

        public int getNumberOfSeats(String courseId)
        {
            return dictionaryCourses[courseId].getNumberOfSeats();
        }

        public String getDataInicio(String courseId)
        {
            return dictionaryCourses[courseId].getDataInicio();
        }


        public Reservation getReservationByCourseAndReservationId(String courseId, String reservationId)
        {
            return dictionaryCourses[courseId].getReservation(reservationId);
        }
    }

}