using System.Collections.Generic;
using System;

namespace CourseApp
{
    public class Course
    {
        String CourseId;
        String Name;
        String Date;
        int NumberOfSeats;

        List<Reservation> Reservations;

        public Course()
        {
        }

        public Course(String CourseId, String Name, String Date, int NumberOfSeats)
        {
            this.CourseId = CourseId;
            this.Name = Name;
            this.Date = Date;
            this.NumberOfSeats = NumberOfSeats;
            Reservations = new List<Reservation>();
        }

        public String getCourseId()
        {
            return CourseId;
        }

        public String getCourseName()
        {
            return Name;
        }

        public int getNumberOfSeats()
        {
            return NumberOfSeats;
        }

        public String createReservation(String nomeEstudante, String emailEstudante)
        {
            Reservation novaReservaVaga = new Reservation(nomeEstudante, emailEstudante);
            Reservations.Add(novaReservaVaga);
            NumberOfSeats--;
            return novaReservaVaga.Id;
        }

        public Reservation getReservation(String ReservationId)
        {
            foreach (Reservation Reservation in Reservations)
            {
                if (Reservation.Id.Equals(ReservationId))
                {
                    return Reservation;
                }
            }
            return null;
        }

        public String getDataInicio()
        {
            return Date;
        }
    }
}