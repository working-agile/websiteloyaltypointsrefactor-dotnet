using System;

namespace CourseApp
{
    public class Reservation
    {

        public string StudentName { set; get; }
        public string StudentEmail { set; get; }
        public string Id { set; get; }

        public Reservation(){}

        public Reservation(string StudentName, string StudentEmail)
        {
            this.StudentName = StudentName;
            this.StudentEmail = StudentEmail;
            Id = "";
        }

        public void setId(string Id)
        {
            this.Id = Id;
        }
    }
}